from pyowm import OWM
from pyowm.utils import timestamps
from datetime import datetime
import requests
import json
import sys


class WeatherHourly:
    def __init__(self, entry):
        self.dt = datetime.fromtimestamp(entry["dt"])
        self.temp = entry["temp"]
        self.humidity = entry["humidity"]
        self.wind_speed = entry["wind_speed"]
        self.weather = entry["weather"]
        self.main = self.weather[0]['main']
        self.description = self.weather[0]['description']

    def printInfo(self):
        print("time:", self.dt)
        print("temperature:", self.temp, "degree")
        print("humidity:", self.humidity)
        print("wind_speed:", self.wind_speed)
        print("Main weather info:", self.main)
        print("Detailed weather info:", self.description)


class WeatherAPIWrapper:
    def __init__(self,
                 api_key,
                 location):
        self._api_key = api_key
        self.owm = OWM(api_key)
        self.mgr = self.owm.weather_manager()
        self.location = location

    def setLocation(self, location):
        self.location = location

    def getLocation(self):
        return self.location

    def getCurrentWeather(self):
        observation = self.mgr.weather_at_place(self.location)
        weather = observation.weather
        return weather

    def getTomorrowForcast(self):
        forcast = self.mgr.forecast_at_place(self.location, '3h')
        tomorrow = timestamps.tomorrow()
        weathers = forcast.get_weather_at(tomorrow)
        return weathers

    def getHourlyWeather(self, lat, lon):
        url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (
            lat, lon, self._api_key)
        response = requests.get(url)
        data = json.loads(response.text)
        hourly = data["hourly"]
        hourly_weather = []

        for entry in hourly:
            hourly_weather.append(WeatherHourly(entry))
        return hourly_weather

    def printWeatherInfo(self, weather):
        print("Current detailed_status: ", weather.detailed_status)
        print("Current wind_status: ")
        print("Speed: ", weather.wind()['speed'])
        print("Degree: ", weather.wind()['deg'])
        print("Current humidity percentage: ", weather.humidity)
        print("Current temperature: ")
        for k, v in weather.temperature('celsius').items():
            print("%s: %s degree" % (k, v))
        if len(weather.rain) > 0:
            print("Current raining_status: ", weather.rain)
        else:
            print("not raining")
        print("Current clouds percentage: ", weather.clouds)