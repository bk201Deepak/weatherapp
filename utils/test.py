import datetime
import time
from time import mktime
from datetime import datetime

import requests
import json
import sys

api_key = "1ec92d299db6ac2337243475cf1f587c"
lat = "37.335480"  # "48.208176"
lon = "-121.893028"  # "16.373819"
url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (lat, lon, api_key)
url2 = "http://api.openweathermap.org/data/2.5/air_pollution?lat=%s&lon=%s&appid=%s" % (lat, lon, api_key)

response2 = requests.get(url2)
data2 = json.loads(response2.text)
print(data2)

aqi_data = data2['list']
print(aqi_data)
print("AQI", aqi_data[0]['main']['aqi'])  # AQI = 1 = Good, 2 = Fair, 3 = Moderate, 4 = Poor, 5 = Very Poor.
print("PM2_5", aqi_data[0]['components']['pm2_5'])
print("PM10", aqi_data[0]['components']['pm10'])


response = requests.get(url)
data = json.loads(response.text)
print(data)
hourly = data["hourly"]

i = 0
for entry in hourly:
    # dt = datetime.fromtimestamp(entry["dt"], pytz.timezone('America/Los_Angeles'))
    dt = datetime.fromtimestamp(entry["dt"])

    time = dt.hour
    temp = entry["temp"]
    humidity = entry["humidity"]
    wind_speed = entry["wind_speed"]
    weather = entry["weather"]
    main = weather[0]['main']
    description = weather[0]['description']

    print(time, temp, humidity, wind_speed, main, description)

    i+=1
    if i==5:
        break

#sys.exit()

# Test 2 --------------------------------------------------------
import pytz as pytz
from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps

# ---------- FREE API KEY examples ---------------------

owm = OWM('1ec92d299db6ac2337243475cf1f587c')
mgr = owm.weather_manager()


# Search for current weather in London (Great Britain) and get details
observation = mgr.weather_at_place('San Jose,US')
w = observation.weather

print(w.detailed_status)         # 'clouds'
print(w.wind())                  # {'speed': 4.6, 'deg': 330}
print("ww>>", w.wind('miles_hour')['speed'])
print(w.humidity )               # 87
print(w.temperature('celsius'))  # {'temp_max': 10.5, 'temp': 9.7, 'temp_min': 9.0}
print(w.temperature('fahrenheit'))
print(w.rain)                    # {}
print(w.heat_index)              # None
print(w.clouds)                  # 75

# Will it be clear tomorrow at this time in Milan (Italy) ?
# forecast = mgr.forecast_at_place('Milan,IT', 'daily')
# answer = forecast.will_be_clear_at(timestamps.tomorrow())
