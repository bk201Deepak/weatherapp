from weatherAPIWrapper import *
import argparse


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('--api_key', type=str,
                    default="none",
                    help='api key')
    ap.add_argument('--org_location', type=str,
                    default="London,GB",
                    help='api key')
    ap.add_argument('--changed_location', type=str,
                    default="Milan,IT",
                    help='api key')
    args = ap.parse_args()
    weatherAPI = WeatherAPIWrapper(args.api_key, args.org_location)
    print("Now test api at location %s" % weatherAPI.getLocation())
    curWeather = weatherAPI.getCurrentWeather()
    weatherAPI.printWeatherInfo(curWeather)
    print()

    weatherAPI.setLocation(args.changed_location)
    print("Now test api at changed location %s" % weatherAPI.getLocation())
    curWeather = weatherAPI.getCurrentWeather()
    weatherAPI.printWeatherInfo(curWeather)
    print()

    print("Now test api for tomorrow weather at changed location %s" % weatherAPI.getLocation())
    tomorrowWeather = weatherAPI.getTomorrowForcast()
    weatherAPI.printWeatherInfo(tomorrowWeather)

    lat = "37.335480"  # "48.208176"
    lon = "-121.893028"  # "16.373819"
    print("Now test the hourly weather at lat = %s, lon = %s" % (lat, lon))
    hourly_weather = weatherAPI.getHourlyWeather(lat, lon)
    for hw in hourly_weather:
        hw.printInfo()