#! /usr/bin/env python

"""
/*-------------------------------------------------------------------------------------------------------------------
*revision file
*Project  : Weather UI
*
*Author   : Deepak Yadav
*Date     : 03/13/2021
* ------------------------------------------------------------------------------------------------------------------*/
"""

REV = 0.1

""""
/*----------------------------------------------------------------------------------------------------------------------
*Rev 0.1   : Started test code to read weather info from openweathermap 
*----------------------------------------------------------------------------------------------------------------------*/
"""