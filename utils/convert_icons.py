"""
/*-------------------------------------------------------------------------------------------------------------------
*                                       CSC520 Final Project
*
*Project  : Weather Dashboard App
*Description: This source files convert the weather icons to required format for gui
*Author   : Deepak Yadav (SID: 89276)
*           Di Zhang (SID: )
*Date     : 03/13/2021
* ------------------------------------------------------------------------------------------------------------------*/
"""
import argparse
import os
import cv2

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('--target_height', type=str,
                    default=95,
                    help='target height')
    ap.add_argument('--target_width', type=str,
                    default=95,
                    help='target height')
    ap.add_argument('--input_path', type=str,
                    default="/Users/dizhang/Documents/itu course/Python Programming/weatherapp/GUI/icons/weather",
                    help='input path for orignal images')
    args = ap.parse_args()

    for filename in os.listdir(args.input_path):
        if "converted.png" in filename:
            continue
        org_image_path = os.path.join(args.input_path, filename)
        output_path = os.path.join(args.input_path, filename.split(".")[0] + "_converted.png")
        img = cv2.imread(org_image_path, cv2.IMREAD_UNCHANGED)

        # replace areas of transparency with white and not transparent
        trans_mask = img[:, :, 3] == 0
        img[trans_mask] = [255, 255, 255, 255]
        new_img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)

        # resize the image to make it smaller
        resized_img = new_img[:]
        s = min(args.target_width, args.target_height)
        resized_img = cv2.resize(new_img, (s, s))
        cv2.imwrite(output_path, resized_img)



