"""
/*-------------------------------------------------------------------------------------------------------------------
*                                       CSC520 Final Project
*
*Project  : Weather Dashboard App
*Description: This source files contains the code for UI widgets and its also the starting point of the program (main)
*Author   : Deepak Yadav (SID: 89276)
*           Di Zhang (SID: 87519738)
*Date     : 03/13/2021
* ------------------------------------------------------------------------------------------------------------------*/
"""

# Import python lib
import json
import queue
import sys
import threading
import time
from datetime import datetime

# Import Mat-plot lib for temp plotting
import numpy as np
import requests
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.ticker import FormatStrFormatter

# Import tk lib
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

# For Getting path to locate icon folder to get images
from pathlib import Path
import os.path

# Backend source files
import weatherAppBackend
import define
import rev


def debug_print(*msg):
    """
    Print debugging messages on debug console
    :param msg: Message to be print
    :return: none
    """

    if define.DebugEN:
        print("UI: ", msg, "\n")


class ThreadedClient:

    def __init__(self, master):
        """
        Start GUI thread and other threads

        :param master: root of the tkinter
        """

        # set root
        self.master = master

        # Override gui window close button
        self.master.protocol('WM_DELETE_WINDOW', self.end_thread)

        # Create  queue to get weather data from backend
        self.humidity_queue = queue.Queue()  # Get Humidity info
        self.wind_queue = queue.Queue()      # Get wind speed info
        self.detailed_status_queue = queue.Queue()     # Get detailed status info
        self.temp_queue = queue.Queue()      # Get Temp  info
        self.tempMax_queue = queue.Queue()   # Get Temp max info
        self.tempMin_queue = queue.Queue()   # Get Temp min info
        self.airQ_queue = queue.Queue()      # Get Air quality (AQI) info

        # set queue in backend code
        weatherAppBackend.set_tk_queue(self.humidity_queue, self.wind_queue, self.detailed_status_queue, self.temp_queue, self.tempMax_queue, self.tempMin_queue, self.airQ_queue)

        # Set Temperature Plot
        self.setup_temp_plot()

        # Start backend Threads to collect weather data from online
        self.back_obj = weatherAppBackend.ThreadClass()

        # Setup GUI widgets
        self.top = Toplevel(master)

        # Setup backend root object
        weatherAppBackend.init(root)

        # Setup non GUI threads
        debug_print("Starting Threads...")
        # Set Thread run flag
        self.running = 1
        # Initialize threads
        self.thread1 = threading.Thread(target=self.humidity_thread, name='humidity_thread')
        self.thread2 = threading.Thread(target=self.wind_thread, name='wind_thread')
        self.thread3 = threading.Thread(target=self.detailed_status_thread, name='detailed_status_thread')
        self.thread4 = threading.Thread(target=self.temp_thread, name='temp_thread')
        self.thread5 = threading.Thread(target=self.temp_max_thread, name='temp_max_thread')
        self.thread6 = threading.Thread(target=self.temp_min_thread, name='temp_min_thread')
        self.thread7 = threading.Thread(target=self.air_quality_thread, name='air_quality_thread')
        self.thread8 = threading.Thread(target=self.date_time_thread, name='date_time_thread')
        # Make thread daemon to run in background- they will close automatically when main GUI is closed
        self.thread1.daemon = True
        self.thread2.daemon = True
        self.thread3.daemon = True
        self.thread4.daemon = True
        self.thread5.daemon = True
        self.thread6.daemon = True
        self.thread7.daemon = True
        self.thread8.daemon = True
        # Start Threads
        self.thread1.start()
        self.thread2.start()
        self.thread3.start()
        self.thread4.start()
        self.thread5.start()
        self.thread6.start()
        self.thread7.start()
        self.thread8.start()

        debug_print("All Thread Started")

    @staticmethod
    def setup_temp_plot():
        global ys
        global temp_c
        global xs
        global line
        global fig
        global x_len
        global ax

        # Number of date points for X axis
        x_len = 5
        # List to hold 5 hours of temp value
        ys = []
        # Range of X axis (1 to 5)
        xs = list(range(1, x_len+1))

        # Send request to get hourly temp date
        response = requests.get(define.weather)
        data = json.loads(response.text)
        hourly = data["hourly"]

        # Get only first 5 hours of temperature
        i = 0
        for entry in hourly:
            dt = datetime.fromtimestamp(entry["dt"])
            # time_h = dt.hour
            # xs.append(time_h)
            temp_h = entry["temp"]
            ys.append(temp_h)
            i += 1

            # Exit loop when first 5 data is collected
            if i == 5:
                break

        # Range of possible Y axis for display [Start: (min_temp - 1C) and End: (max_temp + 1C)]
        y_range = [min(ys) - 1, max(ys) + 1]

        # Create figure for plotting
        fig = plt.figure(facecolor='#f0f0ff')
        ax = fig.add_subplot(1, 1, 1)
        ax.set_ylim(y_range)
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax.yaxis.set_ticks(np.arange(y_range[0], y_range[1], 1.0))

        # Draw plot line
        line, = ax.plot(xs, ys, label="Temp", linewidth=1, color='red')

        # Add labels to plot
        plt.title("Hourly Temperature - 5 hours")
        # plt.xlabel('Samples', fontsize=8)
        plt.ylabel('Temp(C)', fontsize=8)
        # plt.grid()

    def humidity_thread(self):
        """
        This thread get humidity values and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.humidity_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("humidity_thread Thread closed after receiving data in queue")
                break

            self.top.update_humidity_message(value)

        debug_print("humidity_thread closed")

    def wind_thread(self):
        """
        This thread get wind speed values and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.wind_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("wind_thread Thread closed after receiving data in queue")
                break

            self.top.update_wind_message(value)

        debug_print("wind_thread closed")

    def detailed_status_thread(self):
        """
        This thread get detailed status value and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.detailed_status_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("detailed status Thread closed after receiving data in queue")
                break

            self.top.update_detailed_status_message(value)

        debug_print("detailed_status_thread closed")

    def temp_thread(self):
        """
        This thread get temp value and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.temp_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("temp_thread Thread closed after receiving data in queue")
                break

            self.top.update_temp_message(value)

        debug_print("temp_thread closed")

    def temp_max_thread(self):
        """
        This thread get temp max value and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.tempMax_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("temp_max_thread Thread closed after receiving data in queue")
                break

            self.top.update_temp_max_message(value)

        debug_print("temp_max_thread closed")

    def temp_min_thread(self):
        """
        This thread get temp min value and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.tempMin_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("temp_min_thread Thread closed after receiving data in queue")
                break

            self.top.update_temp_min_message(value)

        debug_print("temp_min_thread closed")

    def air_quality_thread(self):
        """
        This thread get air quality value and display it on UI
        :return: None
        """

        while self.running:
            try:
                value = self.airQ_queue.get(block=True)
            except Exception as e:
                continue

            if self.running:
                pass
            else:
                debug_print("air_quality_thread Thread closed after receiving data in queue")
                break

            self.top.update_aqi_message(value)
        debug_print("air_quality_thread closed")

    def date_time_thread(self):
        """
        This Thread gets the current date and time and display it on GUI
        :return: None
        """

        # Get today's day (Monday, Tues,....)
        day = datetime.today().strftime('%A')

        while self.running:
            time_string = day + ", " + time.strftime('%r') + "\n" + time.strftime('%m/%d/%Y ')
            self.top.process_date_time(time_string)
            time.sleep(define.DATE_TIME_THREAD_DELAY)
        debug_print("date_time_thread closed")

    def end_thread(self):
        """
        This function stops all the thread, do some clean up and exit GUI
        """

        # Update Thread run flag to start closing threads
        self.running = 0

        debug_print("Closing Plot...")
        # Close hourly temp plot
        plt.close('all')

        # Close Backend Threads
        debug_print("Closing back end Threads...")
        self.back_obj.end_thread()

        # Send some dummy date to all blocking queues to close all UI thread
        self.humidity_queue.put(0)
        self.wind_queue.put(0)
        self.detailed_status_queue.put(0)
        self.temp_queue.put(0)
        self.tempMax_queue.put(0)
        self.tempMin_queue.put(0)
        self.airQ_queue.put(0)

        # Wait 500ms to let all threads close
        time.sleep(0.5)

        # Fade GUI before closing
        alpha_const = 0.1
        alpha = self.master.attributes("-alpha")
        self.master.attributes("-alpha", alpha)
        for i in range(0, 10):
            alpha -= alpha_const
            self.master.attributes("-alpha", alpha)
            time.sleep(0.1)

        # Close GUI
        debug_print("Closing GUI window...")
        weatherAppBackend.destroy_window()
        debug_print("GUI window closed!")
        sys.exit()


class Toplevel:
    def __init__(self, top=None):
        """
        This class configures and populates the GUI window.
        top is the toplevel containing window.
        """

        # Set GUI frame size and its properties
        top.geometry("660x446+556+266")
        # Remove the maximize button
        top.resizable(0, 0)
        # Set Title of GUI window
        top.title("Weather Dashboard - " + "Rev: " + str(rev.REV))
        # Set Background color
        top.configure(background="#f0f0ff")  # Light blue html color code: f0f0ff

        # Get the path of icons/images
        prog_call = Path().absolute()
        prog_location = str(prog_call) + '/icons'

        # HTML color code for frame title
        FrameTitleColor = '#c6c6ff'

        # Canvas to display plot ---------------------------------------------------------------------------------------
        self.PlotCanvas = tk.Canvas(top)
        self.PlotCanvas = FigureCanvasTkAgg(fig, top)
        self.PlotCanvas.draw()
        self.PlotCanvas.get_tk_widget().place(relx=0.288, rely=0.022, relheight=0.457, relwidth=0.702)

        # Right Side Panel display detail status, temp, date/time and City ----------------------------------------------
        self.Frame1 = tk.Frame(top)
        self.Frame1.place(relx=0.0, rely=0.0, relheight=0.998, relwidth=0.28)
        self.Frame1.configure(relief='flat', borderwidth="2", background="white")

        # Label for displaying image for current weather status
        self.WIconLabel = tk.Label(self.Frame1)
        self.WIconLabel.place(relx=0.054, rely=0.022, height=95, width=170)
        # self.WIconLabel.configure(background="white", foreground="#000000", text='Label')

        # the weather is sunny by default
        photo_location = os.path.join(prog_location, "./weather/ini_converted.png")
        global _img_WIcon
        _img_WIcon = tk.PhotoImage(file=photo_location)
        self.WIconLabel.configure(image=_img_WIcon, background='white')

        # Label for displaying current temp value
        self.TemLabel = tk.Label(self.Frame1, anchor='n')
        self.TemLabel.place(relx=0.054, rely=0.337, height=66, width=120)
        self.TemLabel.configure(background="white", foreground="black", text='12')
        self.TemLabel.configure(font="-family {Segoe UI} -size 40 -weight bold")

        # Label for displaying temp unit image
        self.TemUnitLabel = tk.Label(self.Frame1, anchor='nw')
        self.TemUnitLabel.place(relx=0.664, rely=0.357, height=66, width=45)
        # self.TemUnitLabel.configure(background="white", foreground="black", anchor='nw', text='C')
        # self.TemUnitLabel.configure(font="-family {Segoe UI} -size 12")
        photo_location = os.path.join(prog_location, "./tempUnit.png")
        global _img_tempUnit
        _img_tempUnit = tk.PhotoImage(file=photo_location)
        self.TemUnitLabel.configure(image=_img_tempUnit, background="white", anchor='nw')

        # Label for displaying date and current time
        self.TimeLabel = tk.Label(self.Frame1, anchor='n')
        self.TimeLabel.place(relx=0.054, rely=0.494, height=45, width=160)
        self.TimeLabel.configure(background="white", foreground="black", anchor='n', text='Sunday, 12:59:59 PM')
        # self.TimeLabel.configure(font="-family {Script MT Bold} -size 12 -weight bold")
        self.TimeLabel.configure(font="-family {Segoe UI} -size 11 -weight bold")

        # Label for displaying image for city
        self.LocLabel = tk.Label(self.Frame1, anchor='nw')
        self.LocLabel.place(relx=0.054, rely=0.664, height=130, width=170)
        # self.LocLabel.configure(background="white", foreground="black", text='San Jose CA')
        photo_location = os.path.join(prog_location, "./sjIcon.png")
        global _img_sjIcon
        _img_sjIcon = tk.PhotoImage(file=photo_location)
        self.LocLabel.configure(image=_img_sjIcon, anchor='n')

        # Frame for humidity widgets -----------------------------------------------------------------------------------
        self.HumidtyFrame = tk.Frame(top)
        self.HumidtyFrame.place(relx=0.288, rely=0.516, relheight=0.213, relwidth=0.212)
        self.HumidtyFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying humidity title
        self.HLabel = tk.Label(self.HumidtyFrame)
        self.HLabel.place(relx=0.029, rely=0.021, height=26, width=112)
        self.HLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Humidity %')

        # Label for displaying humidity value
        self.HumidtyValLabel = tk.Label(self.HumidtyFrame)
        self.HumidtyValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.HumidtyValLabel.configure(background="white", foreground="black", text='12')
        self.HumidtyValLabel.configure(font="-family {Segoe UI} -size 24 -weight bold")

        # Frame for wind widgets ---------------------------------------------------------------------------------------
        self.WindFrame = tk.Frame(top)
        self.WindFrame.place(relx=0.53, rely=0.516, relheight=0.213, relwidth=0.212)
        self.WindFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying wind title
        self.WLabel = tk.Label(self.WindFrame)
        self.WLabel.place(relx=0.029, rely=0.021, height=26, width=102)
        self.WLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Wind mph')

        # Label for displaying wind value
        self.WindValLabel = tk.Label(self.WindFrame)
        self.WindValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.WindValLabel.configure(background="white", foreground="black", text='4.7')
        self.WindValLabel.configure(font="-family {Segoe UI} -size 24 -weight bold")

        # Frame for cloud widgets --------------------------------------------------------------------------------------
        self.DetailFrame = tk.Frame(top)
        self.DetailFrame.place(relx=0.773, rely=0.516, relheight=0.213, relwidth=0.212)
        self.DetailFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying Detail title
        self.DetailLabel = tk.Label(self.DetailFrame)
        self.DetailLabel.place(relx=0.029, rely=0.021, height=26, width=102)
        self.DetailLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Detail')

        # Label for displaying Detail value
        self.DetailValLabel = tk.Label(self.DetailFrame)
        self.DetailValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.DetailValLabel.configure(background="white", foreground="black", text='Clear Sky')
        self.DetailValLabel.configure(font="-family {Segoe UI} -size 15 -weight bold")

        # Frame for Max Temp widgets -----------------------------------------------------------------------------------
        self.TempMaxFrame = tk.Frame(top)
        self.TempMaxFrame.place(relx=0.288, rely=0.762, relheight=0.213, relwidth=0.212)
        self.TempMaxFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying max temp title
        self.TmaxLabel = tk.Label(self.TempMaxFrame)
        self.TmaxLabel.place(relx=0.029, rely=0.021, height=26, width=112)
        self.TmaxLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Temp Max')

        # Label for displaying max temp value
        self.TempMaxValLabel = tk.Label(self.TempMaxFrame)
        self.TempMaxValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.TempMaxValLabel.configure(background="white", foreground="black", text='14.0')
        self.TempMaxValLabel.configure(font="-family {Segoe UI} -size 24 -weight bold")

        # Frame for Min Temp widgets -----------------------------------------------------------------------------------
        self.TempMinFrame = tk.Frame(top)
        self.TempMinFrame.place(relx=0.53, rely=0.762, relheight=0.213, relwidth=0.212)
        self.TempMinFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying min temp title
        self.TminLabel = tk.Label(self.TempMinFrame)
        self.TminLabel.place(relx=0.029, rely=0.021, height=26, width=112)
        self.TminLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Temp Min')

        # Label for displaying max temp value
        self.TempMinValLabel = tk.Label(self.TempMinFrame)
        self.TempMinValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.TempMinValLabel.configure(background="white", foreground="black", text='10.56')
        self.TempMinValLabel.configure(font="-family {Segoe UI} -size 24 -weight bold")

        # Frame for air quality widgets --------------------------------------------------------------------------------
        self.AqFrame = tk.Frame(top)
        self.AqFrame.place(relx=0.773, rely=0.762, relheight=0.213, relwidth=0.212)
        self.AqFrame.configure(background="white", borderwidth="2", relief='flat')

        # Label for displaying air quality title
        self.AqLabel = tk.Label(self.AqFrame)
        self.AqLabel.place(relx=0.029, rely=0.021, height=26, width=112)
        self.AqLabel.configure(background="white", foreground=FrameTitleColor, anchor='w', text='Air Quality\nPM2.5 + PM10', justify=tk.LEFT)

        # Label for displaying air quality value
        self.AqValLabel = tk.Label(self.AqFrame)
        self.AqValLabel.place(relx=0.071, rely=0.316, height=56, width=112)
        self.AqValLabel.configure(background="white", foreground="black", text='2')
        self.AqValLabel.configure(font="-family {Segoe UI} -size 24 -weight bold")

    def process_date_time(self, date_time):
        """
        Update time and date label with new value
        :param date_time: Time and date
        :return: None
        """
        self.TimeLabel["text"] = str(date_time)

    def update_humidity_message(self, value):
        """
        Update humidity label with new value
        :param value: humidity value
        :return: None
        """

        try:
            self.HumidtyValLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_humidity_message: ', e)
            pass

    def update_wind_message(self, value):
        """
        Update wind label with new value
        :param value: wind value
        :return: None
        """

        try:
            self.WindValLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_wind_message: ', e)
            pass

    def update_detailed_status_message(self, value):
        """
        Update Detail label with new value
        :param value: Detail value
        :return: None
        """

        try:
            self.DetailValLabel["text"] = str(value)

            # update the weather image according to value
            prog_call = Path().absolute()
            prog_location = str(prog_call) + '/icons'
            if "cloud" in self.DetailValLabel["text"]:
                w = "cloudy"
            elif 'clear' in self.DetailValLabel["text"] or 'sunny' in self.DetailValLabel["text"]:
                w = "sunny"
            elif 'wind' in self.DetailValLabel["text"]:
                w = "windy"
            elif 'snow' in self.DetailValLabel["text"]:
                w = "snowy"
            else:
                w = "rainy"
            photo_location = os.path.join(prog_location, "./weather/%s_converted.png" % w)
            _img_WIcon = tk.PhotoImage(file=photo_location)
            self.WIconLabel.configure(image=_img_WIcon, background='white')
            self.WIconLabel.image = _img_WIcon
        except Exception as e:
            debug_print('ERROR: update detailed status message: ', e)
            pass

    def update_temp_max_message(self, value):
        """
        Update max temp label with new value
        :param value: max temp value
        :return: None
        """

        try:
            self.TempMaxValLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_temp_max_message: ', e)
            pass

    def update_temp_min_message(self, value):
        """
        Update min temp label with new value
        :param value: min temp value
        :return: None
        """

        try:
            self.TempMinValLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_temp_min_message: ', e)
            pass

    def update_aqi_message(self, value):
        """
        Update air quality label with new value
        :param value: air quality value
        :return: None
        """

        try:
            self.AqValLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_aqi_message: ', e)
            pass

    def update_temp_message(self, value):
        """
        Update temp label with new value
        :param value: temp value
        :return: None
        """

        try:
            self.TemLabel["text"] = str(value)
        except Exception as e:
            debug_print('ERROR: update_temp_message: ', e)
            pass


if __name__ == '__main__':
    """
    Main Program - Starting point of program
    """

    # Init tk inter root
    root = tk.Tk()
    # Start ThreadClient to setup thread and and GUI widgets
    client = ThreadedClient(root)
    # Start main loop
    root.mainloop()






