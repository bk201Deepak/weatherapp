"""
/*-------------------------------------------------------------------------------------------------------------------
*                                       CSC520 Final Project
*
*Project  : Weather Dashboard App
*Description: This source files contains some constant values and flags for the application
*Author   : Deepak Yadav (SID: 89276)
*           Di Zhang (SID: 87519738)
*Date     : 03/13/2021
* ------------------------------------------------------------------------------------------------------------------*/
"""

# Flag to enable debugging prints on console
DebugEN = False

# Threads time in seconds
WEATHER_INFO_THREAD    = 5
TEMP_INFO_THREAD       = 5
AQI_THREAD             = 5
DATE_TIME_THREAD_DELAY = 1

# Open Weather map info:
api_key = "1ec92d299db6ac2337243475cf1f587c"
lat     = "37.335480"    # San Jose, CA
lon     = "-121.893028"  # San Jose, CA
weather = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (lat, lon, api_key)
aqi_url = "http://api.openweathermap.org/data/2.5/air_pollution?lat=%s&lon=%s&appid=%s" % (lat, lon, api_key)



