"""
/*-------------------------------------------------------------------------------------------------------------------
*                                       CSC520 Final Project
*
*Project  : Weather Dashboard App
*Description: This source files contains backend code to read weather info from open weather map website:
*             https://openweathermap.org/
*Author   : Deepak Yadav (SID: 89276)
*           Di Zhang (SID: 87519738)
*Date     : 03/13/2021
* ------------------------------------------------------------------------------------------------------------------*/
"""

# Import python lib for threads
import json
import sys
import threading
import time

# Import puowm lib to read weather info
import requests
from pyowm import OWM

# Import Tkinter lib
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

# Import define source file
import define

# Open weather library
try:
    owm = OWM(define.api_key)
    mgr = owm.weather_manager()
except Exception as e:
    print("ERROR: in owm lib")
    sys.exit()


def debug_print(*msg):
    """
    Print debugging messages on debug console
    :param msg: Message to be print
    :return: none
    """

    if define.DebugEN:
        print("B: ", msg, "\n")


def set_tk_queue(queue1, queue2, queue3, queue4, queue5, queue6, queue7):
    """
    Initalize the  queue so that backend code can send data to UI
    :param queue1: Queue for Humidity
    :param queue2: Queue for Wind
    :param queue3: Queue for Cloud
    :param queue4: Queue for Current Temp
    :param queue5: Queue for Max Temp
    :param queue6: Queue for Min Temp
    :param queue7: Queue for Air quality (PM2.5 + PM10)
    :return: None
    """
    global humidity_queue, wind_queue, detailed_status_queue, temp_queue, tempMax_queue, tempMin_queue, airQ_queue

    humidity_queue = queue1
    wind_queue = queue2
    detailed_status_queue = queue3
    temp_queue = queue4

    tempMax_queue = queue5
    tempMin_queue = queue6
    airQ_queue = queue7


class ThreadClass:

    def __init__(self):
        """
        Start backend threads
        """

        # Create Resource LOCK used by threads
        self.Lock = threading.Lock()

        # Set up all threads
        debug_print("Starting H2 Threads...")
        # Set Thread run flag
        self.running = 1
        # Initialize threads
        self.thread1 = threading.Thread(target=self.get_weatherInfo_thread, name='get_weatherInfo_thread')
        self.thread2 = threading.Thread(target=self.aqi_thread, name='aqi_thread')
        self.thread3 = threading.Thread(target=self.get_temp_thread, name='get_temp_thread')
        # Make thread daemon to run in background- they will close automatically when main GUI is closed
        self.thread1.daemon = True
        self.thread2.daemon = True
        self.thread3.daemon = True
        # Start Threads
        self.thread1.start()
        self.thread2.start()
        self.thread3.start()

        debug_print("All backend Thread Started")

    def get_weatherInfo_thread(self):
        """
        This thread will get the weather info from open weather map website
        """

        thread_name = threading.current_thread().name

        while self.running:

            # Get semaphore
            self.Lock.acquire()
            try:
                # Search for current weather in san jose, US and get all details
                observation = mgr.weather_at_place('San Jose,US')
                w = observation.weather

                humidity = w.humidity
                wind = w.wind('miles_hour')['speed']
                detailed_status = w.detailed_status
                temp_val = w.temperature('celsius')
                # temp = temp_val['temp']
                temp_max = temp_val['temp_max']
                temp_min = temp_val['temp_min']

            except Exception as e:
                debug_print(thread_name, "Error: " + str(e))
                wind = 0
                humidity = 0
                detailed_status = 'Clear'
                # temp = 0
                temp_max = 0
                temp_min = 0
            # Release semaphore
            self.Lock.release()

            # Round off wind and temp value to 1 to 2 decimal places
            wind = float("%.2f" % wind)
            # temp = float("%.1f" % temp)
            temp_max = float("%.2f" % temp_max)
            temp_min = float("%.2f" % temp_min)

            # Send data to UI using queue
            humidity_queue.put(humidity)
            wind_queue.put(wind)
            detailed_status_queue.put(detailed_status)
            # temp_queue.put(temp)
            tempMax_queue.put(temp_max)
            tempMin_queue.put(temp_min)

            # Sleep thread
            time.sleep(define.WEATHER_INFO_THREAD)

        debug_print("get_weatherInfo_thread closed")

    def get_temp_thread(self):
        """
        This thread will get the current temperature from open weather map website
        """

        thread_name = threading.current_thread().name

        while self.running:

            # Get semaphore
            self.Lock.acquire()
            try:
                # Search for current weather in san jose, US and get all details
                observation = mgr.weather_at_place('San Jose,US')
                w = observation.weather
                temp_val = w.temperature('celsius')
                temp = temp_val['temp']

            except Exception as e:
                debug_print(thread_name, "Error: " + str(e))
                temp = 0
            # Release semaphore
            self.Lock.release()

            # Round off temp value  to 1 decimal places
            temp = float("%.1f" % temp)

            # Send data to UI using queue
            temp_queue.put(temp)

            # Sleep thread
            time.sleep(define.TEMP_INFO_THREAD)

        debug_print("get_temp_thread closed")

    def aqi_thread(self):
        """
        This thread will get the air quality info from open weather map website
        """

        thread_name = threading.current_thread().name

        while self.running:

            # Get semaphore
            self.Lock.acquire()
            try:
                # Get air quality details for san jose, US
                response = requests.get(define.aqi_url)
                data = json.loads(response.text)
                aqi = data['list']
                air_quality_status = aqi[0]['main']['aqi']  # AQI = 1 = Good, 2 = Fair, 3 = Moderate, 4 = Poor, 5 = Very Poor.

                pm2_5 = aqi[0]['components']['pm2_5']
                pm10 = aqi[0]['components']['pm10']

                air_quality_index = pm2_5 + pm10

            except Exception as e:
                debug_print(thread_name, "Error: " + str(e))
                air_quality_index= 0
            # Release semaphore
            self.Lock.release()

            # Round off wind and temp value to 2 decimal places
            air_quality_index = float("%.2f" % air_quality_index)

            # Send data to UI using queue
            airQ_queue.put(air_quality_index)

            # Sleep thread
            time.sleep(define.AQI_THREAD)

        debug_print("get_weatherInfo_thread closed")

    def end_thread(self):
        """
        This function stops all the thread
        """

        # Disable thread flag
        self.running = 0

        # The above command should close blocking queue : usb thread and user event GUI thread
        debug_print("Wait 500ms to let all thread close...")
        time.sleep(0.5)


def init(top):
    """
    Init UI root used for closing UI
    :param top: root of the tkinter
    :return: None
    """
    global top_level
    top_level = top


def destroy_window():
    """
    Close program
    :return: None
    """
    global top_level
    top_level.destroy()
    top_level = None



