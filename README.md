# Weather Dashboard App
## Python Environment
```bash
pip install -r ./requirements.txt
```
## Running Instruction
```bash
cd ./GUI; python weatherApp.py
```